% hspacing.tex - This document provides examples on how to control
%                horizontal spacing within maths environments.
%
% Andrew Robers - 13th August 2004

\documentclass[a4paper,11pt]{article}
 
    \usepackage{times}
     
    % The mathptmx package does for maths equations what the times package
    % does for the main text. That is, uses scalable fonts rather than the
    % default bitmapped ones. This is useful if you later want to convert
    % your postscript file to PDF.
     
    \usepackage{mathptmx}
    \usepackage{url}
    \usepackage{moreverb}
    
    \begin{document}
    \title{Horizontal spacing in maths}
    \author{Andrew Roberts\\
    \url{http://www.comp.leeds.ac.uk/andyr}}
     
    \date{}
    \maketitle
    
    
    \LaTeX{} is obviously pretty good at typesetting maths --- it was one of
    the chief aims of the core \TeX{} system that \LaTeX{} extends. However, it
    can't always be relied upon to accurately interpret formulae in the way
    you did. It has to make certain assumptions when there are ambiguous
    expressions. The result tends to be slightly incorrect horizontal
    spacing. In these events, the output is still satisfactory, yet, any
    perfectionists will no doubt wish to \emph{fine-tune} there formulae to
    ensure spacing is correct. These are generally very subtle adjustments.
    
    There are other occasions where \LaTeX{} has done its job correctly, but
    you just want to add some space, maybe to add a comment of some kind. For
    example:
    
    
    \[f(n) = \left\{ 
    \begin{array}{l l}
      n/2 & \quad \mbox{if $n$ is even}\\
      -(n+1)/2 & \quad \mbox{if $n$ is odd}\\ \end{array} \right. \]
    
    It's preferable in this example to ensure there is a decent amount of
    space between the maths and the text. This was achieved using the
    following code:
    
    \footnotesize
    \begin{verbatim}
    \[f(n) = \left\{ 
    \begin{array}{l l}
      n/2 & \quad \mbox{if $n$ is even}\\
      -(n+1)/2 & \quad \mbox{if $n$ is odd}\\ \end{array} \right. \]
    \end{verbatim}
    
    \normalsize
    
    \LaTeX{} has defined two commands that can be used anywhere in documents
    (not just maths) to insert some horizontal space. They are:
    
    \begin{center}
      \begin{tabular}{ c c }
        \verb|\quad| & \verb|\qquad| 
      \end{tabular}
    \end{center}
    
    A \texttt{\textbackslash quad} is a space equal to the current font
    size. So, if you are using an 11pt font, then the space provided by  
    \texttt{\textbackslash quad} will be 11pt (horizontally, of course.) The
    \texttt{\textbackslash qquad} gives twice that amount. As you can see
    from the code from the above example, \texttt{\textbackslash quad}s were
    used to add some separation between the maths and the text.
    
    Ok, so back to the fine tuning as mentioned at the beginning of the
    document. A good example would be displaying the simple equation for the
    indefinite integral of $y$ with respect to $x$:
    
    \[ \int y\; \mathrm{d}x \]
    
    If you were to try this, you may write:
    
    \begin{verbatim}
    \[ \int y \mathrm{d}x \]
    \end{verbatim}
    
    Which gives,
    
    \[ \int y \mathrm{d}x \]
    
    However, this doesn't give the correct result. \LaTeX{} doesn't respect
    the whitespace left in the code to signify that the $y$ and the
    $\mathrm{d}x$ are independent entities. Instead, it lumps them
    altogether. A \texttt{\textbackslash quad} would clearly be overkill is
    this situation --- what is needed are some small spaces to be utilised
    in this type of instance, and that's what \LaTeX{} provides:
    
    \begin{center}
      \begin{tabular}{ l l l }
        \textbf{Command} & \textbf{Description} & \textbf{Size} \\
        \verb|\,| & small space & $3/18$ of a quad \\
        \verb|\:| & medium space & $4/18$ of a quad \\
        \verb|\;| & large space & $5/18$ of a quad \\
        \verb|\!| & negative space & $-3/18$ of a quad \\
      \end{tabular}
    \end{center}
    
    NB you can use more than one command in a sequence to achieve a greater
    space if necessary. 
    
    To rectify the current problem:
    
    \begin{eqnarray*}
    \verb|\int y\, \mathrm{d}x| & \displaystyle{\int y\, \mathrm{d}x} \\
    \verb|\int y\: \mathrm{d}x| & \displaystyle{\int y\: \mathrm{d}x} \\
    \verb|\int y\; \mathrm{d}x| & \displaystyle{\int y\; \mathrm{d}x}
    \end{eqnarray*}
    
    The negative space may seem like an odd thing to use, however, it
    wouldn't be there if it didn't have \emph{some} use! Take the following
    example:
    
    \[\left(
    \begin{array}{c}
      n \\
      r
      \end{array}\right) = {^n}C_r = {^n}C_r = \frac{n!}{r!(n-r)!}\]
    
    The matrix-like expression for representing binomial coefficients is too
    padded. There is too much space between the brackets and the actual
    contents within. This can easily be corrected by adding a few negative
    spaces after the left bracket and before the right bracket.
    
    \[\left(\!\!\!
    \begin{array}{c}
      n \\
      r
      \end{array}\!\!\!\right) = {^n}C_r = \frac{n!}{r!(n-r)!}\]
    
    \newpage
    
    \begin{verbatim}
    \[\left(\!\!\!
      \begin{array}{c}
        n \\
        r
      \end{array}
      \!\!\!\right) = {^n}C_r = \frac{n!}{r!(n-r)!}
    \]
    \end{verbatim}
    \end{document}