% textineqn.tex - Illustrating how to add text to equations.
%
% Andrew Roberts - 4th August 2004

\documentclass[a4paper]{article}

  \usepackage{times}
  
  % The mathptmx package does for maths equations what the times package
  % does for the main text. That is, uses scalable fonts rather than the
  % default bitmapped ones. This is useful if you later want to convert
  % your postscript file to PDF.
  
  \usepackage{mathptmx}
  \usepackage{url}
  
  \begin{document}
  \title{Adding text to equations}
  \author{Andrew Roberts\\
  \url{http://www.comp.leeds.ac.uk/andyr}}
  
  \date{}
  \maketitle
  
  \section{Basic text}
  
  I doubt it will be every day that you will need to include some text
  within an equation. However, sometimes it needs to be done. Just
  sticking the text straight in the maths environment won't give you the
  results you want. For example:
  
  \begin{verbatim}
  \begin{equation}
    50 apples \times 100 apples = lots of apples
  \end{equation}
  \end{verbatim}
  
  Gives,
  
  \begin{equation}
    50 apples \times 100 apples = lots of apples
  \end{equation}
  
  There are two noticeable problems. Firstly, there are no spaces between
  numbers and text, nor spaces between multiple words. Secondly, the words
  don't look quite right --- the letters are more spaced out than normal.
  Both issues are simply artifacts of the maths mode, in that it doesn't
  expect to see words. Any spaces that you type in maths mode are ignored
  and \LaTeX{} spaces elements according to its own rules. It is assumed
  that any characters represent variable names. To emphasise that each
  symbol is an individual, they are not positioned as closely together as
  with normal text.
  
  There are a number of ways that text can be added properly. The typical
  way is to wrap the text with the \verb|\mbox{...}| command. This command
  hasn't been introduced before, however, it's job is basically to create
  a text box just width enough to contain the supplied text. Text within
  this box cannot be broken across lines. Let's see what happens when the
  above equation code is adapted:
  
  \begin{equation}
    50 \mbox{apples} \times 100 \mbox{apples} = \mbox{lots of apples}
  \end{equation}
  
  The text looks better. However, there are no gaps between the numbers
  and the words. Unfortunately, you are required to explicitly add these.
  There are many ways to add spaces between maths elements, however, for
  the sake of simplicity, I find it easier, in this instance at least,
  just to literally add the space character in the affected
  \verb|\mbox|(s) itself (just before the text.)
  
  \begin{verbatim}
  \begin{equation}
    50 \mbox{ apples} \times 100 \mbox{ apples} = 
    \mbox{lots of apples}
  \end{equation}
  \end{verbatim}
  
  \begin{equation}
    50 \mbox{ apples} \times 100 \mbox{ apples} = \mbox{lots of apples}
  \end{equation}
  
  
  \section{Formatted text}
  
  Using the \verb|\mbox| is fine and gets the basic result. Yet, there is
  an alternative that offers a little more flexibility. You may recall
  from Tutorial 7 ---
  Formatting\footnote{\url{http://www.comp.leeds.ac.uk/andyr/misc/latex/latextutorial7.html}}
  the introduction of font formatting commands, such as \verb|\textrm|,
  \verb|\textit|, \verb|\textbf|, etc. These commands format the argument
  accordingly, e.g., \verb|\textbf{bold text}| gives \textbf{bold text}.
  These commands are equally valid within a maths environment to include
  text. The added benefit here is that you can have better control over
  the font formatting, rather than the standard text achieved with
  \verb|\mbox|.
  
  \begin{verbatim}
  \begin{equation}
    50 \textrm{ apples} \times 100 \textbf{ apples} = 
    \textit{lots of apples}
  \end{equation}
  \end{verbatim}
  
  
  \begin{equation}
    50 \textrm{ apples} \times 100 \textbf{ apples} = \textit{lots of apples}
  \end{equation}
  
  
  However, as is the case with \LaTeX{}, there is more than one way to skin
  a cat! There are a set of formatting commands very similar to the font
  formatting ones just used, except they are aimed specifically for text
  in maths mode. So why bother showing you \verb|\textrm| and co if there
  are equivalents for maths? Well, that's because they are subtly
  different. The maths formatting commands are:
  
  \begin{center}
  \begin{tabular}{ l l l }
    Command & Format & Example \\
    \verb|\mathrm| & Roman & $\mathrm{e = mc^2}$ \\
    \verb|\mathit| & Italic & $\mathit{e = mc^2}$ \\
    \verb|\mathbf| & Bold & $\mathbf{e = mc^2}$ \\
    \verb|\mathsf| & Sans serif & $\mathsf{e = mc^2}$ \\
    \verb|\mathtt| & Typewriter & $\mathtt{e = mc^2}$ \\
    \verb|\mathcal| & Calligraphy & $\mathcal{e = mc^2}$ \\
  \end{tabular}
  \end{center}
  
  The maths formatting commands can be wrapped around the entire equation,
  and not just on the textual elements: they only format letters, numbers,
  and uppercase Greek, and the rest of the maths syntax is ignored. So,
  generally, it is better to use the specific maths commands if required.
  Note that the calligraphy example gives rather strange output. This is
  because for letters, it requires upper case characters. The reminding
  letters are mapped to special symbols.
  
  \end{document}