% longeqns.tex - Latex document illustrating how to cope with long
%                formulas
%
% Andrew Robers - 6 August 2004

\documentclass[a4paper]{article}
 
    \usepackage{times}
     
    % The mathptmx package does for maths equations what the times package
    % does for the main text. That is, uses scalable fonts rather than the
    % default bitmapped ones. This is useful if you later want to convert
    % your postscript file to PDF.
     
    \usepackage{mathptmx}
    \usepackage{url}
    \usepackage{moreverb}
    
    \begin{document}
    \title{Dealing with long equations}
    \author{Andrew Roberts\\
    \url{http://www.comp.leeds.ac.uk/andyr}}
     
    \date{}
    \maketitle
    
    An example of a long formula:
    
    \begin{equation}
    \left(1+x\right)^n = 1 + nx + \frac{n\left(n-1\right)}{2!}x^2 +
    \frac{n\left(n-1\right)\left(n-2\right)}{3!}x^3 +
    \frac{n\left(n-1\right)\left(n-2\right)\left(n-3\right)}{4!}x^4 + \ldots
    \end{equation}
    
    \LaTeX{} doesn't break long equations to make them fit within the margins
    as it does with normal text. It is therefore up to you to format the
    equation appropriately (if they overrun the margin.) This typically
    requires some creative use of an \texttt{eqnarray} to get elements
    shifted to a new line to align nicely.
    
    \footnotesize
    \begin{verbatim}
    \begin{eqnarray*}
      \left(1+x\right)^n & = & 1 + nx + \frac{n\left(n-1\right)}{2!}x^2 \\
      & & + \frac{n\left(n-1\right)\left(n-2\right)}{3!}x^3 \\
      & & + \frac{n\left(n-1\right)\left(n-2\right)\left(n-3\right)}{4!}x^4 \\
      & & + \ldots
    \end{eqnarray*}
    \end{verbatim}
    
    \normalsize
    \begin{eqnarray*}
      \left(1+x\right)^n & = & 1 + nx + \frac{n\left(n-1\right)}{2!}x^2 \\
      & & + \frac{n\left(n-1\right)\left(n-2\right)}{3!}x^3 \\
      & & + \frac{n\left(n-1\right)\left(n-2\right)\left(n-3\right)}{4!}x^4 \\
      & & + \ldots
    \end{eqnarray*}
    
    It may just be that I'm more sensitive to these kind of things, but you
    may notice that from the 2nd line onwards, the space between the initial
    plus sign and the subsequent fraction is (slightly) smaller than normal.
    (Observe the first line, for example.) This is due to the fact that
    \LaTeX{} deals with the $+$ and $-$ signs in two possible ways. The most
    common is as a binary operator. When two maths elements appear either
    side of the sign, it is assumed to be a binary operator, and as such,
    allocates some space either side of the sign. The alternative way is a
    sign designation. This is when you state whether a mathematical quantity
    is either positive or negative. This is common for the latter, as in
    maths, such elements are assumed to be positive unless a $-$ is prefixed
    to it. In this instance, you want the sign to appear close to the
    appropriate element to show their association. It is this interpretation
    that \LaTeX{} has opted for in the above example.
    
    To add the correct amount of space, you can add an \textit{invisible}
    character using \texttt{\{\}}, as illustrated here:
    
    \newpage
    \footnotesize
    \begin{verbatim}
    \begin{eqnarray*}
      \left(1+x\right)^n & = & 1 + nx + \frac{n\left(n-1\right)}{2!}x^2 \\
      & & {} + \frac{n\left(n-1\right)\left(n-2\right)}{3!}x^3 \\
      & & {} + \frac{n\left(n-1\right)\left(n-2\right)\left(n-3\right)}{4!}x^4 \\
      & & {} + \ldots
    \end{eqnarray*}
    \end{verbatim}
    
    \normalsize
    
    \begin{eqnarray*}
      \left(1+x\right)^n & = & 1 + nx + \frac{n\left(n-1\right)}{2!}x^2 \\
      & & {} + \frac{n\left(n-1\right)\left(n-2\right)}{3!}x^3 \\
      & & {} + \frac{n\left(n-1\right)\left(n-2\right)\left(n-3\right)}{4!}x^4 \\
      & & {} + \ldots
    \end{eqnarray*}
    
    Alternatively, you could avoid this issue altogether by leaving the $+$
    at the end of the previous line rather at the beginning of the current
    line:
    
    \begin{eqnarray*}
      \left(1+x\right)^n & = & 1 + nx + \frac{n\left(n-1\right)}{2!}x^2 + \\
      & & \frac{n\left(n-1\right)\left(n-2\right)}{3!}x^3 + \\
      & & \frac{n\left(n-1\right)\left(n-2\right)\left(n-3\right)}{4!}x^4 + \\
      & & \ldots
    \end{eqnarray*}
    
    There is another convention of writing long equations that \LaTeX{}
    supports. This is the way I see long equations typeset in books and
    articles, and admittedly is my preferred way of displaying them.
    Sticking with the \texttt{eqnarray} approach, using the
    \texttt{\textbackslash lefteqn} command around the content before the
    $=$ sign gives the following result:
    
    \begin{eqnarray*}
      \lefteqn{\left(1+x\right)^n = } \\
      & & 1 + nx + \frac{n\left(n-1\right)}{2!}x^2 + \\
      & & \frac{n\left(n-1\right)\left(n-2\right)}{3!}x^3 + \\
      & & \frac{n\left(n-1\right)\left(n-2\right)\left(n-3\right)}{4!}x^4 + \\
      & & \ldots
    \end{eqnarray*}
    
    \footnotesize
    \begin{verbatim}
    \begin{eqnarray*}
      \lefteqn{\left(1+x\right)^n = } \\
      & & 1 + nx + \frac{n\left(n-1\right)}{2!}x^2 + \\
      & & \frac{n\left(n-1\right)\left(n-2\right)}{3!}x^3 + \\
      & & \frac{n\left(n-1\right)\left(n-2\right)\left(n-3\right)}{4!}x^4 + \\
      & & \ldots
    \end{eqnarray*}
    \end{verbatim}
    \normalsize
    
    Notice that the first line of the \texttt{eqnarray} contains the
    \texttt{\textbackslash lefteqn} only. And within this command, there are
    no column separators (\&). The reason this command displays things as it
    does is because the \texttt{\textbackslash lefteqn} prints the argument,
    however, tells \LaTeX{} that the width is zero. This results in the first
    column being empty, with the exception of the inter-column space, which
    is what gives the subsequent lines their indentation.
    
    \end{document}