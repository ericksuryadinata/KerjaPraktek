\babel@toc {english}{}
\contentsline {chapter}{Halaman Pengesahan}{i}
\contentsline {chapter}{Kata Pengantar}{ii}
\contentsline {chapter}{Daftar Isi}{iv}
\contentsline {chapter}{Daftar Gambar}{v}
\contentsline {chapter}{Daftar Tabel}{vi}
\contentsline {chapter}{\numberline {I}PENDAHULUAN}{1}
\contentsline {section}{\numberline {1.1}Latar Belakang}{1}
\contentsline {section}{\numberline {1.2}Tujuan}{1}
\contentsline {section}{\numberline {1.3}Manfaat}{2}
\contentsline {section}{\numberline {1.4}Waktu dan Lokasi Kerja Praktek}{3}
\contentsline {subsection}{\numberline {1.4.1}Waktu}{3}
\contentsline {subsection}{\numberline {1.4.2}Lokasi}{3}
\contentsline {chapter}{\numberline {II}GAMBARAN UMUM}{4}
\contentsline {section}{\numberline {2.1}Sejarah Organisasi / Perusahaan}{4}
\contentsline {section}{\numberline {2.2}Struktur Organisasi}{5}
\contentsline {section}{\numberline {2.3}Tugas dan Fungsi}{5}
\contentsline {section}{\numberline {2.4}Denah Lokasi}{9}
\contentsline {chapter}{\numberline {III}PELAKSANAAN KERJA PRAKTEK}{10}
\contentsline {section}{\numberline {3.1}Codeigniter}{10}
\contentsline {section}{\numberline {3.2}Composer}{12}
\contentsline {section}{\numberline {3.3}GIT}{13}
\contentsline {section}{\numberline {3.4}Proses Bisnis}{13}
\contentsline {section}{\numberline {3.5}Simulasi UNBK - Rasyiidu Indonesia}{13}
\contentsline {chapter}{\numberline {IV}KESIMPULAN DAN SARAN}{15}
\contentsline {section}{\numberline {4.1}Kesimpulan}{15}
\contentsline {chapter}{DAFTAR PUSTAKA}{16}
